from spell import *
import sys
import os

# Specify symbols to remove and strip and replace and and and
sr = ["\n", ",", ".", "?", "!", ":", ";", "'"]
ss = ["_", "*", "#"] + [" "]
sre = ['"']

# Function that cleans word
def clean(s):
    word = s
    for x in sr:
        word = word.replace(x, "")
    for x in sre:
        word = word.replace(x, " ")
    for x in ss:
        word = word.strip(x)
    return word

# Read input file
with open(sys.argv[1], "r") as fp:
    text = fp.read()

# Loop over words
split_text = [""] + text.split(" ") + [""]
for index, i in enumerate(split_text):
    # If not first or last word, which are actually buffers
    if index not in [0, len(split_text)-1]:

        # Get cleaned word
        word = clean(i)
        
        # Get all possible spellings with a function from spell.py
        c = list(candidates(word.lower()))

        # Check if word is allready correct or has no correction options
        if len(c) == 1:
            continue
        elif word not in ["", " "] and not word[0].isupper():
            # Otherwise, prompt the user to confirm spelling after clearing
            os.system("clear")

            # Get words before and after, as well as cleaned versions
            before = split_text[index-1]
            after = split_text[index+1]

            cleaned_before = clean(before)
            cleaned_after = clean(after)

            # Print word
            print "Erroneous word:\t{}\n\n".format(word)

            # Print context
            print "Context:\t{} {} {}\n\n\n".format(cleaned_before, word, cleaned_after)

            # List possible alternatives with index
            print "Possible alternatives:\n"
            for xindex, x in enumerate(c):
                print "{}. {}".format(xindex, x)

            # Get choice from user and write
            print "\n\n\n\nType a number, leave blank for no replacement, or type '+' to add word to personal dictionary"
            choice = raw_input("> ")

            if choice == "":
                continue

            elif choice == "+":
                with open("words.txt", "a") as fp:
                    fp.write("\n" + word)
            
            # Replace words
            else:
                text.replace("{} {} {}".format(before, i, after), "{} {} {}".format(before, c[int(choice)], after))

# If output file specified, write to that file. Otherwise, write over input file.
if len(sys.argv) > 2:
    fname = sys.argv[2]
else:
    fname = sys.argv[1]

with open(fname, "w") as fp:
    fp.write(text)
