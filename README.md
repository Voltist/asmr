# ASMR - A Spellcheck for Markdown thats pretty Rad

A simple spell check system for markdown files, in python 2.7


## Getting started

It's pretty easy, just run asmr.py with the markdown filename as an argument

```
python asmr.py test.md
```

Or specify an output file

```
python asmr.py test.md out.md
```

**Note: You must use python2**

**Another Note: There will be errors and it will make silly mistakes, please dont use it mindlessly**


## Why 'ASMR'?

As I was thinking about creating this simple program I was talking to a friend about LaTeX, which is an equally silly name.
